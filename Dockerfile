# This file is a template, and might need editing before it works on your project.
# German Telecom image they test with
FROM python:3.9.6-alpine3.13



WORKDIR /usr/src/app



COPY . /usr/src/app

# For Django
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
# CMD ["python", "app.py"]
